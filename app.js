var pusher = new Pusher('test123', {
  wsHost: "54.164.144.11",
  wsPort: "8080",
  wssPort: "8080",
  enabledTransports: ['ws', 'flash']
});

console.log("connected?");
var channel = pusher.subscribe("test");
channel.bind('my_event', function(data) {
  $("#chat-box").append("<p>" + data.message + "</p>")
  console.log('An event was triggered with message: ' + data.message);
});
